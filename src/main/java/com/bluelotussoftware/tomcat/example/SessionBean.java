package com.bluelotussoftware.tomcat.example;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 */
@ManagedBean
@SessionScoped
public class SessionBean implements Serializable {

    private static final long serialVersionUID = -8482236318166960736L;

    public SessionBean() {
    }

    /**
     * Logs the current user out by invalidating the session.
     *
     * @return &quot;logout&quot; which is used by the
     * {@literal faces-config.xml} to redirect back to the
     * {@literal index.xhtml} page.
     */
    public String logout() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.invalidateSession();
        return "/index";
    }
}
